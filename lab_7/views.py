from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Friend
from .csui_helper import CSUIhelper
# import os

# by guhkun
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

response = {}
csui_helper = CSUIhelper()

def index(request):
    response['author']= "guhkunganteng"
    response['friend_list'] = Friend.objects.all()
    all_mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    html = 'lab_7/lab_7.html'

    #untuk pagination : https://docs.djangoproject.com/en/1.11/topics/pagination/
    paginator = Paginator(all_mahasiswa_list, 10)
    page = request.GET.get('page')
    try:
        mahasiswa_list = paginator.page(page)
    except PageNotAnInteger:
        mahasiswa_list = paginator.page(1)
    except EmptyPage:
        mahasiswa_list = paginator.page(paginator.num_pages)

    response['mahasiswa_list'] = mahasiswa_list
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request): # update
    print ("method friend list json")
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({"results": friends}, content_type='application/json')

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        return JsonResponse(friend.as_dict())

@csrf_exempt
def delete_friend(request):
    print ("call delete friend")
    friend_id = request.POST['friend_id']
    print ("friend_id = ", friend_id)
    st = Friend.objects.get(npm=friend_id).delete()
    print ("st = ", st)

    return HttpResponseRedirect('/lab-7/')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)
